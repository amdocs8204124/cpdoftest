package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;
 
public class TestClassTest {
   @Test
    public void teststring2() throws Exception {
 
        String result= new com.agiletestingalliance.TestClass("hello world").gstr();
        assertTrue("the description is incorrect", result.contains("world"));
    }
}