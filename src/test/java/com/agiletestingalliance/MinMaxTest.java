package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;
public class MinMaxTest {
   @Test
    public void testF() throws Exception {
        int k= new MinMax().f(2,4);
        assertEquals("max",4,k);
    }
	@Test
    public void testF1() throws Exception {
        int k= new MinMax().f(4,2);
        assertEquals("max",4,k);
    }
	@Test
    public void testBar() throws Exception {
        String k= new MinMax().bar("");
        assertEquals("max","",k);
    }
}